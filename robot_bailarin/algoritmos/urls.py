from django.conf.urls import url, patterns

urlpatterns = patterns(
    '',
    url(r'^$', 'algoritmos.views.index', name='algoritmos_index'),
)