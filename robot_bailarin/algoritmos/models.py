from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Paso(models.Model):
    nombre = models.CharField(verbose_name=_("Nombre"), max_length=100)
    
    class Meta:
        verbose_name = _("Paso")
        verbose_name_plural = _("Pasos")
        
        
class Algoritmo(models.Model):
    nombre = models.CharField(verbose_name=_("Nombre"), max_length=100)
    pasos = models.ManyToManyField(Paso, verbose_name=_("Pasos"))
    
    class Meta:
        verbose_name = _("Algoritmo")
        verbose_name_plural = _("Algoritmos")