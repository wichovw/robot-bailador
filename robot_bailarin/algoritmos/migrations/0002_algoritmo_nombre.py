# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('algoritmos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='algoritmo',
            name='nombre',
            field=models.CharField(default='Nuevo Algoritmo', max_length=100, verbose_name='Nombre'),
            preserve_default=False,
        ),
    ]
