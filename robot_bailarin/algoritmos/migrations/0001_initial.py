# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Algoritmo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'Algoritmo',
                'verbose_name_plural': 'Algoritmos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Paso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100, verbose_name='Nombre')),
            ],
            options={
                'verbose_name': 'Paso',
                'verbose_name_plural': 'Pasos',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='algoritmo',
            name='pasos',
            field=models.ManyToManyField(verbose_name='Pasos', to='algoritmos.Paso'),
            preserve_default=True,
        ),
    ]
