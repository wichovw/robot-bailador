from django.conf.urls import patterns, include, url
from django.contrib import admin
from algoritmos import views as algoritmos_views

urlpatterns = patterns(
    '',
    (r'^algoritmo/', include('algoritmos.urls')),
)
